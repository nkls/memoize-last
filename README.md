Simple memoization function that only remembers the last result.

The comparison of arguments is made using referential equality checks. This type
of comparison is very fast, but requires you to not mutate your arguments. This
makes it a great fit for React applications.

# Installation

With yarn:

```
yarn add memoize-last
```

With npm:

```
npm install memoize-last
```

# Usage

It exports both a named export and a default one. They both point to the same
function, so you can import it either like this:

```js
import { memoizeLast } from 'memoize-last';
```

... or like this:

```js
import memoizeLast from 'memoize-last';
```

## Basic

```js
const addMemoized = memoizeLast((a, b) => {
  console.log('Ran it!');
  return a + b;
});

addMemoized(1, 2); // 3
// Log: Ran it!

addMemoized(1, 2); // 3
// ... crickets..

addMemoized(0, 3); // 3
// Log: Ran it!
```

## With React (and class fields)

This library was originally written specifically for this use case. By using
memoization you can call all of your render related logic from inside your
render function without worrying about performance. This makes you render logic
declarative and easy to understand.

```jsx
class List extends Component {
  state = {
    names: ['Johan', 'Veronica', 'Patric']
  };

  sortNames = memoizeLast(names => names.sort((a, b) => (a < b ? -1 : 1)));

  withoutNamesStartingWithJ = memoizeLast(names =>
    names.filter(name => name[0] !== 'J')
  );

  render() {
    // If this.state.names didn't change between renders then this operation will
    // return the exact same array as last time and this operation will be
    // practically free...
    const sortedNames = this.sortNames(this.state.names);

    // ...and if sortNames return the exact same list then this operation will
    // also be free
    const namesNotStartingWithJ = this.withoutNamesStartingWithJ(sortedNames);

    return (
      <ul>{namesNotStartingWithJ.map(name => <li key={name}>{name}</li>)}</ul>
    );
  }
}
```

([class fields](https://github.com/tc39/proposal-class-fields))

## Note on referential equality

This library only does referential equality checks internally. This means that
if you mutate an object and pass that same object again, the memoized function
will not call your passed function and you won't receive the expected result:

```js
const getName = memoizeLast(user => user.name);
const person = { name: 'Adam' };

getName(person); // Adam

person.name = 'Eva';

getName(person); // Adam  <-- oops!
```

If you need deep checks you should check out the memoization function of
underscore or lodash instead.

## Typescript

This module is written in Typescript and therefor provides type definitions out
of the box. It will pass through the type of the wrapped function, so the type
of `memoizeLast(myFunc)` will be the same as `myFunc`.
