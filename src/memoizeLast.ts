// tslint:disable:no-any
export function memoizeLast<R, T extends (...args: any[]) => R>(fn: T): T {
  let lastArgs: any[];
  let lastResult: any;

  const memoized = (...args: any[]) => {
    if (
      lastArgs &&
      args.length === lastArgs.length &&
      args.every(function(arg: any, i: number) {
        return arg === lastArgs[i];
      })
    ) {
      return lastResult;
    }

    lastArgs = args;
    lastResult = fn(...args);

    return lastResult;
  };

  return memoized as T;
}

export default memoizeLast;
