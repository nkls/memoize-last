import defaultExport, { memoizeLast } from '../';
import 'jest';

test('default and name export is the same object', () => {
  expect(defaultExport).toBe(memoizeLast);
});

test('kitchen sink', () => {
  const func = jest.fn((arg1, arg2) => {
    return arg1 + arg2;
  }) as (arg1: number, arg2: number) => number;

  const memoized = memoizeLast(func);

  expect(memoized(1, 2)).toBe(3);
  expect(func).toHaveBeenCalledTimes(1);
  expect(memoized(1, 2)).toBe(3);
  expect(func).toHaveBeenCalledTimes(1);
  expect(memoized(1, 3)).toBe(4);
  expect(func).toHaveBeenCalledTimes(2);
});
